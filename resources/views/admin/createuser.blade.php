@extends('layouts.dashboard')
@section('page_heading','Admin')

@section('section')

    {{ Form::open(['method' => 'post', 'class' => 'form-horizontal']) }}
    <div class="form-group">
        <div class="col-sm-6">
            {{ Form::label('foto', 'Foto', ['class' => 'control-label']) }}
            {{ Form::file('foto', null) }}
        </div>
        <div class="form-group">
            <div class="col-sm-6">
                <a href="admin">
                    <button type="button" class="btn btn-primary">
                    <i class="fa fa-arrow-left"></i> Terug
                    </button>
                </a>
            </div>
        </div>
        <div class="col-sm-6">
            {{ Form::label('voornaam', 'Voornaam', ['class' => 'control-label']) }}
            {{ Form::text('voornaam', null, ['class' => 'form-control', 'placeholder' => 'Voornaam']) }}
        </div>
        <div class="col-sm-6">
            {{ Form::label('achternaam', 'Achternaam', ['class' => 'control-label']) }}
            {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Achternaam']) }}
        </div>
        <div class="col-sm-6">
            {{ Form::label('functie', 'Functie', ['class' => 'control-label']) }}
            {{ Form::text('functie', null, ['class' => 'form-control', 'placeholder' => 'Functie']) }}
        </div>
        <div class="col-sm-6">
            {{ Form::label('geboortedatum', 'Geboortedatum', ['class' => 'control-label']) }}
            {{ Form::text('geboortedatum', null, ['class' => 'form-control', 'placeholder' => 'Geboortedatum']) }}
        </div>
        <div class="col-sm-6">
            {{ Form::label('straat', 'Straat + Huisnummer', ['class' => 'control-label']) }}
            {{ Form::text('straat', null, ['class' => 'form-control', 'placeholder' => 'Straat + Huisnummer']) }}
        </div>
        <div class="col-sm-6">
            {{ Form::label('woonplaats', 'Woonplaats', ['class' => 'control-label']) }}
            {{ Form::text('woonplaats', null, ['class' => 'form-control', 'placeholder' => 'Woonplaats']) }}
        </div>
        <div class="col-sm-6">
            {{ Form::label('email', 'E-mail', ['class' => 'control-label']) }}
            {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'E-mail']) }}
        </div>
        <div class="col-sm-6">
            {{ Form::label('telefoonnummer', 'Telefoonnummer', ['class' => 'control-label']) }}
            {{ Form::text('telefoonnummer', null, ['class' => 'form-control', 'placeholder' => 'Telefoonnummer']) }}
        </div>
        <div class="col-sm-6">
            {{ Form::label('gebruikersnaam', 'Gebruikersnaam', ['class' => 'control-label']) }}
            {{ Form::text('gebruikersnaam', null, ['class' => 'form-control', 'placeholder' => 'Gebruikersnaam']) }}
        </div>
        <div class="col-sm-6">
            {{ Form::label('wachtwoord', 'Wachtwoord', ['class' => 'control-label']) }}
            {{ Form::text('wachtwoord', null, ['class' => 'form-control', 'placeholder' => 'Wachtwoord']) }}
        </div>
        <div class="col-sm-6">
            {{ Form::label('wachtwoord', 'Herhaal Wachtwoord', ['class' => 'control-label']) }}
            {{ Form::text('wachtwoord', null, ['class' => 'form-control', 'placeholder' => 'Herhaal Wachtwoord']) }}
        </div>
    </div>
    <div>

        <div class="form-group">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-check"></i> Gebruiker toevoegen
                </button>
            </div>
        </div>
    </div>
    {{ Form::close() }}

@endsection
@include('widgets.panel', array('header'=>true, 'as'=>'cotable'))

@stop